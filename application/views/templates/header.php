<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> <?= $title; ?> | Worship Arts Network</title>

    <link rel="stylesheet" href="<?php echo base_url();?>assets/materialize/css/materialize.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/styles.min.css">
</head>
<body>

    <section class="main-wrapper">